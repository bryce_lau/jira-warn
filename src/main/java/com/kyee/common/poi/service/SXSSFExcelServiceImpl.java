package com.kyee.common.poi.service;

import com.kyee.common.poi.IExcelService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Copyright (C), kyee 2016-2016
 * Author : liuxiaoxin
 * Date : 2017/10/31
 * Description :
 * Version : 1.0
 */
@Service("SXSSFExcelServiceImpl")
public class SXSSFExcelServiceImpl implements IExcelService {
    public final static String SUFFIX = "xlsx";
    public final static int MAX_ROW = 1048576;
    public static String DEFAULT_DATE_PATTERN = "yyyy/MM/dd";//默认日期格式
    public static String HEAD_PLACEHOLDER = "#Col";

    /**
     * @param file
     * @param offsetX
     * @param offsetY
     * @return
     * @throws Exception
     */
    @Override
    public List<Object[]> loadExcelData(MultipartFile file, int offsetX, int offsetY) throws Exception {
        //检查文件
        checkFile(file);
        //获得Workbook工作薄对象
        XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
        //创建返回对象，把每行中的值作为一个数组，所有行作为一个集合返回
        List<Object[]> list = new ArrayList<>();
        Iterator<Sheet> sheets = workbook.iterator();
        while (sheets.hasNext()) {
            Sheet sheet = sheets.next();
            Iterator<Row> rows = sheet.rowIterator();
            while (rows.hasNext()) {
                Row row = rows.next();
                if (row.getRowNum() < offsetY)
                    continue;
                int cloumns = getExcelCloumns(sheet);
                if (offsetX > 0) {
                    cloumns = cloumns - offsetX;
                }
                Object[] data = new Object[cloumns];
                for (int i = 0; i < cloumns; i++) {
                    Cell cell = row.getCell(i);
                    data[i] = getCellValue(cell);
                }
                list.add(data);
            }
        }
        workbook.close();
        return list;
    }


    /**
     * 导出Excel 2007 OOXML (.xlsx)格式
     *
     * @param title
     * @param headers
     * @param cloumns
     * @param datas
     * @param out
     */
    @Override
    public void exportExcel(String title, String[] headers, int[] cloumns, List<Object[]> datas, OutputStream out) throws Exception {
        // 声明一个工作薄
        SXSSFWorkbook workbook = new SXSSFWorkbook(1000);//缓存
        workbook.setCompressTempFiles(true);
        // 生成一个(带标题)表格
        SXSSFSheet sheet = workbook.createSheet();
        int rowcount = 0;
        if (StringUtils.isNotEmpty(title)) rowcount++;
        if (headers.length > 0) rowcount++;

        if (datas.size() == 0) {
            createTitleCell(workbook, sheet, title, headers);
            createHeadCell(workbook, sheet, title, headers, cloumns);
        } else {
            List<Object[]>[] lists = spiltList(datas, MAX_ROW - rowcount);
            for (int i = 0; i < lists.length; i++) {
                if (i > 0) {
                    sheet = workbook.createSheet();
                }
                createTitleCell(workbook, sheet, title, headers);
                createHeadCell(workbook, sheet, title, headers, cloumns);
                workbook.setSheetName(i, "第" + (i + 1) + "页");
                setCellValue(lists[i], sheet, rowcount, null, workbook);

            }
        }
        workbook.write(out);
        workbook.close();
        workbook.dispose();
    }

    public void downLoadExcel(String filName, String title, String[] headers, int[] cloumns, List<Object[]> excelData) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            File myFile = new File("config/out/" + filName+simpleDateFormat.format(new Date())+".xlsx");
            if(!myFile.exists())
                myFile.createNewFile();
            OutputStream output = new FileOutputStream(myFile);
            exportExcel(title, headers, cloumns, excelData, output);
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void exportExcelWithMultiHeaders(String title, List<String[]> headers, int[] cloumns, String[] aligns, List<Object[]> datas, OutputStream out) throws Exception {
        SXSSFWorkbook workbook = new SXSSFWorkbook(1000);//缓存
        workbook.setCompressTempFiles(true);
        // 生成一个(带标题)表格
        SXSSFSheet sheet = workbook.createSheet();
        int rowcount = 0;
        if (StringUtils.isNotEmpty(title)) rowcount++;
        if (CollectionUtils.isNotEmpty(headers)) rowcount += headers.size();

        if (datas.size() == 0) {
            createTitleCell(workbook, sheet, title, CollectionUtils.isNotEmpty(headers) ? headers.get(0) : new String[]{""});
            if (CollectionUtils.isNotEmpty(headers)) {
                createMultiHeadCell(workbook, sheet, title, headers, cloumns);
                mergedRegionMultiHeaders(sheet, title, headers);
            }
        } else {
            List<Object[]>[] lists = spiltList(datas, MAX_ROW - rowcount);
            for (int i = 0; i < lists.length; i++) {
                if (i > 0) {
                    sheet = workbook.createSheet();
                }
                createTitleCell(workbook, sheet, title, CollectionUtils.isNotEmpty(headers) ? headers.get(0) : new String[]{""});
                if (CollectionUtils.isNotEmpty(headers)) {
                    createMultiHeadCell(workbook, sheet, title, headers, cloumns);
                }
                workbook.setSheetName(i, "第" + (i + 1) + "页");
                setCellValue(lists[i], sheet, rowcount, aligns, workbook);
                mergedRegionMultiHeaders(sheet, title, headers);
            }
        }
        workbook.write(out);
        workbook.close();
        workbook.dispose();
    }

    private void setCellValue(List<Object[]> dataList, SXSSFSheet sheet, int rowcount, String[] aligns, SXSSFWorkbook workbook) {
        for (int j = 0; j < dataList.size(); j++) {
            SXSSFRow dataRow = sheet.createRow(j + rowcount);
            Object[] data = dataList.get(j);
            for (int t = 0; t < data.length; t++) {
                SXSSFCell newCell = dataRow.createCell(t);
                Object o = data[t];
                String cellValue = "";
                if (o == null) cellValue = "";
                else if (o instanceof Date) cellValue = new SimpleDateFormat(DEFAULT_DATE_PATTERN).format(o);
                else if (o instanceof Float || o instanceof Double)
                    cellValue = new BigDecimal(o.toString()).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
                else cellValue = o.toString();

                newCell.setCellValue(cellValue);
                if (null != aligns && aligns.length > t) {
                    newCell.setCellStyle(getCellStyle(workbook, aligns[t]));
                }

            }
        }
    }

    /**
     * @param list
     * @param length
     * @return
     */
    private List[] spiltList(List list, int length) {
        List[] lists = new List[(list.size() / length) + 1];
        for (int i = 0; i < lists.length; i++) {
            lists[i] = list.subList(i * length, ((i + 1) * length > list.size() ? list.size() : length * (i + 1)));
        }
        return lists;
    }

    /**
     * @param sheet
     * @param title
     * @param header
     */
    private void createTitleCell(SXSSFWorkbook workbook, SXSSFSheet sheet, String title, String[] header) {
        if (StringUtils.isNotEmpty(title)) {
            SXSSFRow titleRow = sheet.createRow(0);//表头
            titleRow.createCell(0).setCellValue(title);
            titleRow.getCell(0).setCellStyle(getTitleCellStyle(workbook));
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, header.length - 1));
        }
    }

    /**
     * 创建复合表头
     *
     * @param sheet
     * @param title
     * @param headers
     */
    private void mergedRegionMultiHeaders(SXSSFSheet sheet, String title, List<String[]> headers) {
        int firstRow = StringUtils.isNotEmpty(title) ? 1 : 0;
        if (CollectionUtils.isNotEmpty(headers)) {
            int maxCol = headers.get(0).length - 1;
            int maxRow = firstRow + headers.size() - 1;
            Map<Integer, List<Integer>> allVerticalMap = new HashMap<>();
            for (int i = 0; i < headers.size(); i++) {
                int row = firstRow + i;
                List<Integer> hasList = new ArrayList<>();
                for (int j = 0; j < headers.get(i).length; j++) {
                    if (!ObjectUtils.isEmpty(headers.get(i)[j]) && !HEAD_PLACEHOLDER.equals(headers.get(i)[j])) {
                        hasList.add(j);
                    }
                    List<Integer> verticalList = new ArrayList<>();
                    for (int kk = 0; kk < headers.size(); kk++) {
                        if (!ObjectUtils.isEmpty(headers.get(kk)[j]) && !HEAD_PLACEHOLDER.equals(headers.get(kk)[j])) {
                            verticalList.add(firstRow + kk);
                        }
                    }
                    if (CollectionUtils.isNotEmpty(verticalList)) {
                        allVerticalMap.put(j, verticalList);
                    }
                }
                hasList.stream().forEach(ii -> {
                    List<Integer> list = hasList.stream().filter(kk -> kk > ii).collect(Collectors.toList());
                    if (CollectionUtils.isNotEmpty(list)) {
                        int nextValue = Collections.min(list);
                        if (nextValue - 1 > ii) {
                            sheet.addMergedRegion(new CellRangeAddress(row, row, ii, nextValue - 1));
                        }
                    } else {
                        if (ii < maxCol) {
                            sheet.addMergedRegion(new CellRangeAddress(row, row, ii, maxCol));
                        }
                    }
                });
            }
            allVerticalMap.forEach((key, list) -> {
                for (int i = 0; i < list.size(); i++) {
                    int startRow = firstRow + i;
                    List<Integer> vlist = list.stream().filter(kk -> kk > startRow).collect(Collectors.toList());
                    if (CollectionUtils.isNotEmpty(vlist)) {
                        int nextValue = Collections.min(vlist);
                        if (nextValue - 1 > startRow) {
                            sheet.addMergedRegion(new CellRangeAddress(startRow, nextValue - 1, key, key));
                        }
                    } else {
                        if (startRow < maxRow)
                            sheet.addMergedRegion(new CellRangeAddress(startRow, maxRow, key, key));
                    }
                }
            });
        }
    }

    /**
     * @param sheet
     * @param header
     */
    private void createHeadCell(SXSSFWorkbook workbook, SXSSFSheet sheet, String title, String[] header, int[] cloumn) {

        if (header.length > 0) {
            SXSSFRow headerRow = sheet.createRow(StringUtils.isNotEmpty(title) ? 1 : 0);
            for (int i = 0; i < header.length; i++) {
                headerRow.createCell(i).setCellValue(header[i]);
                headerRow.getCell(i).setCellStyle(getHeaderCellStyle(workbook));
                if (cloumn.length > i) {
                    sheet.setColumnWidth(i, cloumn[i]);
                }

            }
        }

    }

    /**
     * @param sheet
     * @param header
     */
    private void createMultiHeadCell(SXSSFWorkbook workbook, SXSSFSheet sheet, String title, List<String[]> header, int[] cloumn) {
        if (CollectionUtils.isNotEmpty(header)) {
            int startRow = StringUtils.isNotEmpty(title) ? 1 : 0;
            for (int i = 0; i < header.size(); i++) {
                SXSSFRow headerRow = sheet.createRow(startRow + i);
                for (int j = 0; j < header.get(i).length; j++) {
                    headerRow.createCell(j).setCellValue(header.get(i)[j]);
                    headerRow.getCell(j).setCellStyle(getHeaderCellStyle(workbook));
                    if (cloumn.length > j) {
                        sheet.setColumnWidth(j, cloumn[j]);
                    }
                }
            }
        }


    }

    /**
     * @param file
     * @throws IOException
     */
    private void checkFile(MultipartFile file) throws IOException {
        //判断文件是否存在
        if (null == file) {
            throw new FileNotFoundException("文件不存在！");
        }
        //获得文件名
        String fileName = file.getOriginalFilename();
        //判断文件是否是excel文件
        if (!fileName.endsWith(SUFFIX)) {
            throw new IOException(fileName + "不是excel文件");
        }
    }


    /**
     * @param file
     * @return
     * @throws Exception
     */
    private Workbook getWorkBook(MultipartFile file) throws Exception {
        //获得文件名
        String fileName = file.getOriginalFilename();
        //创建Workbook工作薄对象，表示整个excel
        Workbook workbook = null;
        //获取excel文件的io流
        InputStream is = file.getInputStream();
        //根据文件后缀名不同(xls和xlsx)获得不同的Workbook实现类对象
        workbook = new XSSFWorkbook(is);
        return workbook;
    }

    private CellStyle getTitleCellStyle(SXSSFWorkbook workbook) {
        CellStyle titleStyle = workbook.createCellStyle();
        titleStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
        titleStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
        Font titleFont = workbook.createFont();
        titleFont.setFontHeightInPoints((short) 14);
        titleFont.setBoldweight((short) 700);
        titleStyle.setFont(titleFont);
        return titleStyle;
    }

    private CellStyle getHeaderCellStyle(SXSSFWorkbook workbook) {
        CellStyle titleStyle = workbook.createCellStyle();
        titleStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
        titleStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
        Font titleFont = workbook.createFont();
        titleFont.setFontHeightInPoints((short) 10);
        titleFont.setBoldweight((short) 700);
        titleStyle.setFont(titleFont);
        titleStyle.setBorderBottom(BorderStyle.THIN);
        titleStyle.setBorderLeft(BorderStyle.THIN);
        titleStyle.setBorderRight(BorderStyle.THIN);
        titleStyle.setBorderTop(BorderStyle.THIN);
        return titleStyle;
    }

    private CellStyle getCellStyle(SXSSFWorkbook workbook, String align) {
        CellStyle titleStyle = workbook.createCellStyle();
        switch (align) {
            case "left":
                titleStyle.setAlignment(HSSFCellStyle.ALIGN_LEFT);
                break;
            case "center":
                titleStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
                break;
            case "right":
                titleStyle.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
                break;
            default:
                break;
        }
        titleStyle.setBorderBottom(BorderStyle.THIN);
        titleStyle.setBorderLeft(BorderStyle.THIN);
        titleStyle.setBorderRight(BorderStyle.THIN);
        titleStyle.setBorderTop(BorderStyle.THIN);
        Font titleFont = workbook.createFont();
        return titleStyle;
    }
}
