package com.kyee.common.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Copyright (C), kyee 2016-2016
 * Author : liuxiaoxin
 * Date : 2017/10/31
 * Description :
 * Version : 1.0
 */

public interface IExcelService {

    /**
     * @param fileName 完整文件名
     * @param title    标题
     * @param headers  列头
     * @param cloumns  列宽
     * @param datas    数据源
     * @param response
     * @throws Exception
     */
    default void downLoadExcelFile(String fileName, String title, String[] headers, int[] cloumns, List<Object[]> datas, HttpServletResponse response) throws Exception {
        OutputStream output = response.getOutputStream();
        response.reset();
        response.setHeader("Content-Disposition", "attachment;filename=\"" + new String((fileName).getBytes("gbk"), "iso-8859-1") + "\"");
        response.setContentType("application/msexcel");
        exportExcel(title, headers, cloumns, datas, output);
        output.close();
    }

    default void downLoadExcelFileWithMultiHeaders(String fileName, String title, List<String[]> headers, int[] cloumns, String[] aligns, List<Object[]> datas, HttpServletResponse response) throws Exception {
        response.reset();
        OutputStream output = response.getOutputStream();
        response.setHeader("Content-Disposition", "attachment;filename=\"" + new String((fileName).getBytes("gbk"), "iso-8859-1") + "\"");
        response.setContentType("application/msexcel");
        exportExcelWithMultiHeaders(title, headers, cloumns, aligns, datas, output);
        output.close();
    }


    /**
     * 获取单元格类型
     *
     * @param cell
     * @return
     */
    default String getCellValue(Cell cell) {
        String cellValue = "";
        if (cell == null) {
            return cellValue;
        }
        //把数字当成String来读，避免出现1读成1.0的情况
        if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            cell.setCellType(Cell.CELL_TYPE_STRING);
        }
        //判断数据的类型
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_NUMERIC: //数字
                cellValue = String.valueOf(cell.getNumericCellValue());
                break;
            case Cell.CELL_TYPE_STRING: //字符串
                cellValue = String.valueOf(cell.getStringCellValue());
                break;
            case Cell.CELL_TYPE_BOOLEAN: //Boolean
                cellValue = String.valueOf(cell.getBooleanCellValue());
                break;
            case Cell.CELL_TYPE_FORMULA: //公式
                cellValue = String.valueOf(cell.getCellFormula());
                break;
            case Cell.CELL_TYPE_BLANK: //空值
                cellValue = "";
                break;
            case Cell.CELL_TYPE_ERROR: //故障
                cellValue = "非法字符";
                break;
            default:
                cellValue = "未知类型";
                break;
        }
        return cellValue;
    }

    /**
     * 获取数量
     *
     * @param sheet
     * @return
     */
    default int getExcelCloumns(Sheet sheet) {
        List<Integer> cloumns = new ArrayList<>();
        Iterator<Row> rows = sheet.rowIterator(); //获得第一个表单的迭代器
        while (rows.hasNext()) {
            Row row = rows.next();  //获得行数据
            cloumns.add(row.getPhysicalNumberOfCells());
        }
        cloumns.sort((i, j) -> j - i);
        return cloumns.size() > 0 ? cloumns.get(0) : 0;
    }

    /**
     * @param file
     * @param offsetX
     * @param offsetY
     * @return
     * @throws Exception
     */
    List<Object[]> loadExcelData(MultipartFile file, int offsetX, int offsetY) throws Exception;

    /**
     * @param title
     * @param headers
     * @param cloumns
     * @param datas
     * @param out
     * @throws Exception
     */
    void exportExcel(String title, String[] headers, int[] cloumns, List<Object[]> datas, OutputStream out) throws Exception;

    /**
     * 复合表头导出
     *
     * @param title
     * @param headers
     * @param cloumns
     * @param datas
     * @param out
     * @throws Exception
     */
    void exportExcelWithMultiHeaders(String title, List<String[]> headers, int[] cloumns, String[] aligns, List<Object[]> datas, OutputStream out) throws Exception;

}
