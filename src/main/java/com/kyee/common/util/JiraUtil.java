package com.kyee.common.util;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.joda.time.DateTime;
import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.NullProgressMonitor;
import com.atlassian.jira.rest.client.domain.BasicComponent;
import com.atlassian.jira.rest.client.domain.Comment;
import com.atlassian.jira.rest.client.domain.Issue;
import com.atlassian.jira.rest.client.internal.jersey.JerseyJiraRestClientFactory;
import com.kyee.entity.JiraInfoModel;

import mjson.Json;

public class JiraUtil {

    /**
     * 登录JIRA并返回指定的JiraRestClient对象
     *
     * @param username
     * @param password
     * @return
     * @throws URISyntaxException
     */
    public static JiraRestClient login_jira(String uri,String username, String password) throws URISyntaxException {
        try {
            final JerseyJiraRestClientFactory factory = new JerseyJiraRestClientFactory();
            final URI jiraServerUri = new URI(uri);
            final JiraRestClient restClient = factory.createWithBasicHttpAuthentication(jiraServerUri, username,
                    password);
            return restClient;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取并返回指定的Issue对象
     *
     * @param issueNum
     * @param username
     * @param password
     * @return
     * @throws URISyntaxException
     */
    public static Issue get_issue(String uri,String issueNum, String username, String password) throws URISyntaxException {
        try {
            final JiraRestClient restClient = login_jira(uri,username, password);
            final NullProgressMonitor pm = new NullProgressMonitor();
            final Issue issue = restClient.getIssueClient().getIssue(issueNum, pm);
            return issue;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA备注部分的内容
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static List<String> get_comments_body(Issue issue) throws URISyntaxException {
        try {
            List<String> comments = new ArrayList<String>();
            for (Comment comment : issue.getComments()) {
                comments.add(comment.getBody().toString());
            }
            return comments;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的创建时间
     *
     * @return
     * @throws URISyntaxException
     */
    public static DateTime get_create_time(Issue issue) throws URISyntaxException {
        try {
            return issue.getCreationDate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的描述部分
     *
     * @return
     * @throws URISyntaxException
     */
    public static String get_description(Issue issue) throws URISyntaxException {
        try {
            return issue.getDescription();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的标题
     *
     * @return
     * @throws URISyntaxException
     */
    public static String get_summary(Issue issue) throws URISyntaxException {
        try {
            return issue.getSummary();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的报告人的名字
     *
     * @return
     * @throws URISyntaxException
     */
    public static String get_reporter(Issue issue) throws URISyntaxException {
        try {
            return issue.getReporter().getDisplayName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的分派人的姓名列表
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static ArrayList<String> get_assignees(Issue issue) throws URISyntaxException {
        try {
            Json json = Json.read(issue.getFieldByName("分派给").getValue().toString());
            Iterator<Json> assignees = json.asJsonList().iterator();
            ArrayList<String> assigneesNames = new ArrayList<String>();
            while (assignees.hasNext()) {
                Json assignee = Json.read(assignees.next().toString());
                String assigneeName = assignee.at("displayName").toString();
                assigneesNames.add(assigneeName);
            }
            return assigneesNames;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的状态
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_status(Issue issue) throws URISyntaxException {
        try {
            return issue.getStatus().getName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的类型
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_issue_type(Issue issue) throws URISyntaxException {
        try {
            return issue.getIssueType().getName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的模块
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static ArrayList<String> get_modules(Issue issue) throws URISyntaxException {
        try {
            ArrayList<String> arrayList = new ArrayList<String>();
            Iterator<BasicComponent> basicComponents = issue.getComponents().iterator();
            while (basicComponents.hasNext()) {
                String moduleName = basicComponents.next().getName();
                arrayList.add(moduleName);
            }
            return arrayList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的前端人员的姓名列表
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static ArrayList<String> get_qianduans(Issue issue) throws URISyntaxException {
        try {
            ArrayList<String> qianduanList = new ArrayList<String>();
            Json json = Json.read(issue.getFieldByName("前端").getValue().toString());
            Iterator<Json> qianduans = json.asJsonList().iterator();
            while (qianduans.hasNext()) {
                Json qianduan = Json.read(qianduans.next().toString());
                String qianduanName = qianduan.at("displayName").toString();
                qianduanList.add(qianduanName);
            }
            return qianduanList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的开发的姓名列表
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static ArrayList<String> get_developers(Issue issue) throws URISyntaxException {
        try {
            ArrayList<String> developersList = new ArrayList<String>();
            Json json = Json.read(issue.getFieldByName("开发").getValue().toString());
            Iterator<Json> developers = json.asJsonList().iterator();
            while (developers.hasNext()) {
                Json developer = Json.read(developers.next().toString());
                String developerName = developer.at("displayName").toString();
                developersList.add(developerName);
            }
            return developersList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的产品人员的姓名
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_product(Issue issue) throws URISyntaxException {
        try {
            String product_field = issue.getFieldByName("产品人员").getValue().toString();
            return Json.read(product_field).at("displayName").toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的UE开始时间
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_UE_start_time(Issue issue) throws URISyntaxException {
        try {
            String UE_start_time = issue.getFieldByName("UE开始时间").getValue().toString();
            return UE_start_time;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的UE结束时间
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_UE_end_time(Issue issue) throws URISyntaxException {
        try {
            String UE_end_time = issue.getFieldByName("UE结束时间").getValue().toString();
            return UE_end_time;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的UI开始时间
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_UI_start_time(Issue issue) throws URISyntaxException {
        try {
            String UI_start_time = issue.getFieldByName("UI开始时间").getValue().toString();
            return UI_start_time;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的UI结束时间
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_UI_end_time(Issue issue) throws URISyntaxException {
        try {
            String UI_end_time = issue.getFieldByName("UI结束时间").getValue().toString();
            return UI_end_time;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的客户端开始时间
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_app_start_time(Issue issue) throws URISyntaxException {
        try {
            String app_start_time = issue.getFieldByName("客户端开始时间").getValue().toString();
            return app_start_time;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的客户端结束时间
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_app_end_time(Issue issue) throws URISyntaxException {
        try {
            String app_end_time = issue.getFieldByName("客户端结束时间").getValue().toString();
            return app_end_time;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的前端开始时间
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_qianduan_start_time(Issue issue) throws URISyntaxException {
        try {
            String qianduan_start_time = issue.getFieldByName("前端开始时间").getValue().toString();
            return qianduan_start_time;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的前端结束时间
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_qianduan_end_time(Issue issue) throws URISyntaxException {
        try {
            String qianduan_end_time = issue.getFieldByName("前端结束时间").getValue().toString();
            return qianduan_end_time;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的开发开始时间
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_develop_start_time(Issue issue) throws URISyntaxException {
        try {
            String develop_start_time = issue.getFieldByName("开发开始时间").getValue().toString();
            return develop_start_time;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的开发结束时间
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_develop_end_time(Issue issue) throws URISyntaxException {
        try {
            String develop_end_time = issue.getFieldByName("开发结束时间").getValue().toString();
            return develop_end_time;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的联调开始时间
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_liantiao_start_time(Issue issue) throws URISyntaxException {
        try {
            String liantiao_start_time = issue.getFieldByName("联调开始时间").getValue().toString();
            return liantiao_start_time;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的联调结束时间
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_liantiao_end_time(Issue issue) throws URISyntaxException {
        try {
            String liantiao_end_time = issue.getFieldByName("联调结束时间").getValue().toString();
            return liantiao_end_time;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的测试开始时间
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_test_start_time(Issue issue) throws URISyntaxException {
        try {
            String test_start_time = issue.getFieldByName("测试开始时间").getValue().toString();
            return test_start_time;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取指定JIRA的测试结束时间
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static String get_test_end_time(Issue issue) throws URISyntaxException {
        try {
            String test_end_time = issue.getFieldByName("测试结束时间").getValue().toString();
            return test_end_time;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取所有可以收集到的JIRA信息并返回JiraInfoModel类型对象
     *
     * @param issue
     * @return
     * @throws URISyntaxException
     */
    public static JiraInfoModel get_jira_info(Issue issue) throws URISyntaxException {
        List<String> jiraCommentsBody = get_comments_body(issue);
        DateTime jiraCreateTime = get_create_time(issue);
        String description = get_description(issue);
        String summary = get_summary(issue);
        String reporter = get_reporter(issue);
        ArrayList<String> assignees = get_assignees(issue);
        String status = get_status(issue);
        String issueType = get_issue_type(issue);
        ArrayList<String> modules = get_modules(issue);
        ArrayList<String> qianduans = get_qianduans(issue);
        ArrayList<String> developers = get_developers(issue);
        String product = get_product(issue);
        String UE_start_time = get_UE_start_time(issue);
        String UE_end_time = get_UE_end_time(issue);
        String UI_start_time = get_UI_start_time(issue);
        String UI_end_time = get_UI_end_time(issue);
        String app_start_time = get_app_start_time(issue);
        String app_end_time = get_app_end_time(issue);
        String qianduan_start_time = get_qianduan_start_time(issue);
        String qianduan_end_time = get_qianduan_end_time(issue);
        String develop_start_time = get_develop_start_time(issue);
        String develop_end_time = get_develop_end_time(issue);
        String liantiao_start_time = get_liantiao_start_time(issue);
        String liantiao_end_time = get_liantiao_end_time(issue);
        String test_start_time = get_test_start_time(issue);
        String test_end_time = get_test_end_time(issue);
        JiraInfoModel jiraInfoModel = new JiraInfoModel();
        jiraInfoModel.setJiraCommentsBody(jiraCommentsBody);
        jiraInfoModel.setJiraCreateTime(jiraCreateTime);
        jiraInfoModel.setDescription(description);
        jiraInfoModel.setSummary(summary);
        jiraInfoModel.setReporter(reporter);
        jiraInfoModel.setAssignees(assignees);
        jiraInfoModel.setStatus(status);
        jiraInfoModel.setIssueType(issueType);
        jiraInfoModel.setModules(modules);
        jiraInfoModel.setQianduans(qianduans);
        jiraInfoModel.setDevelopers(developers);
        jiraInfoModel.setProduct(product);
        jiraInfoModel.setUE_start_time(UE_start_time);
        jiraInfoModel.setUE_end_time(UE_end_time);
        jiraInfoModel.setUI_start_time(UI_start_time);
        jiraInfoModel.setUI_end_time(UI_end_time);
        jiraInfoModel.setApp_start_time(app_start_time);
        jiraInfoModel.setApp_end_time(app_end_time);
        jiraInfoModel.setQianduan_start_time(qianduan_start_time);
        jiraInfoModel.setQianduan_end_time(qianduan_end_time);
        jiraInfoModel.setDevelop_start_time(develop_start_time);
        jiraInfoModel.setDevelop_end_time(develop_end_time);
        jiraInfoModel.setLiantiao_start_time(liantiao_start_time);
        jiraInfoModel.setLiantiao_end_time(liantiao_end_time);
        jiraInfoModel.setTest_start_time(test_start_time);
        jiraInfoModel.setTest_end_time(test_end_time);
        return jiraInfoModel;
    }

}