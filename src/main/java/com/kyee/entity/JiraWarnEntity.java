package com.kyee.entity;

public class JiraWarnEntity {
    private String readStatus;
    private String warnStatus;
    private String jiraMessage;

    public String getJiraMessage() {
        return jiraMessage;
    }

    public void setJiraMessage(String jiraMessage) {
        this.jiraMessage = jiraMessage;
    }

    public String getWarnStatus() {
        return warnStatus;
    }

    public void setWarnStatus(String warnStatus) {
        this.warnStatus = warnStatus;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }
}
