package com.kyee.entity;

/**
 * Created by Administrator on 2018/1/17.
 */
public class Sheet {
    private String sheetNo;
    private String summary;
    private String[] fixVersions;
    private String modifyPerson;
    private String modifyPersonName;
    private String groupName;
    private String remark;
    private String module;
    private String fixVersion;
    private boolean inRule = false;

    public String getModifyPerson() {
        return modifyPerson;
    }

    public void setModifyPerson(String modifyPerson) {
        this.modifyPerson = modifyPerson;
    }

    public String[] getFixVersions() {
        return fixVersions;
    }

    public void setFixVersions(String[] fixVersions) {
        this.fixVersions = fixVersions;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSheetNo() {
        return sheetNo;
    }

    public void setSheetNo(String sheetNo) {
        this.sheetNo = sheetNo;
    }

    public boolean isInRule() {
        return inRule;
    }

    public void setInRule(boolean inRule) {
        this.inRule = inRule;
    }

    public String getModifyPersonName() {
        return modifyPersonName;
    }

    public void setModifyPersonName(String modifyPersonName) {
        this.modifyPersonName = modifyPersonName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public String getFixVersion() {
        return fixVersion;
    }

    public void setFixVersion(String fixVersion) {
        this.fixVersion = fixVersion;
    }
}
