package com.kyee.entity;

import javax.swing.*;

/**
 * Created by Administrator on 2018/1/19.
 */
public class FrameEntity {
    private int width = 400;//窗体宽度
    private int height = 280;//窗体高度
    private int stayTime = 5;//休眠时间
    private String title;
    private String readMessage;
    private String message;//消息标题,内容
    private int style = JRootPane.PLAIN_DIALOG;//窗体样式


    public int getStyle() {
        return style;
    }

    public void setStyle(int style) {
        this.style = style;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getStayTime() {
        return stayTime;
    }

    public void setStayTime(int stayTime) {
        this.stayTime = stayTime;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getReadMessage() {
        return readMessage;
    }

    public void setReadMessage(String readMessage) {
        this.readMessage = readMessage;
    }
}
