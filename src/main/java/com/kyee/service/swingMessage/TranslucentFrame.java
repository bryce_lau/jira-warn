package com.kyee.service.swingMessage;

import com.baidu.aip.speech.AipSpeech;
import com.baidu.aip.speech.TtsResponse;
import com.kyee.entity.FrameEntity;
import com.sun.awt.AWTUtilities;
import javazoom.jl.player.Player;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;

/**
 * Created by BryceLau on 2018/1/19.
 * 绘制右下角提示信息
 */
@Component
public class TranslucentFrame extends Applet {

    public static final String APP_ID = "10720934";
    public static final String API_KEY = "KIGrclmERmHTcfVyiF1R9jrv";
    public static final String SECRET_KEY = "XpcFhWgrUCtbPI4P8MrnqKWZQEGK6ykI";


    JFrame frame;
    JLabel label1;
    JEditorPane editorPane1;

    public void drawFrame(FrameEntity frameEntity) {
        frame = new JFrame();
        editorPane1 = new JEditorPane();
        editorPane1.setEditable(false);
        editorPane1.setContentType("text/html");
        editorPane1.setText(frameEntity.getMessage());
        frame.add(editorPane1);
        frame.setTitle(frameEntity.getTitle());
        //设置窗体的位置及大小
        int x = Toolkit.getDefaultToolkit().getScreenSize().width - Toolkit.getDefaultToolkit().getScreenInsets(frame.getGraphicsConfiguration()).right - frameEntity.getWidth() - 5;
        int y = Toolkit.getDefaultToolkit().getScreenSize().height - Toolkit.getDefaultToolkit().getScreenInsets(frame.getGraphicsConfiguration()).bottom - frameEntity.getHeight() - 5;
        frame.setBounds(x, y, frameEntity.getWidth(), frameEntity.getHeight());
        frame.setUndecorated(true); // 去掉窗口的装饰
        frame.getRootPane().setWindowDecorationStyle(frameEntity.getStyle()); //窗体样式
        AWTUtilities.setWindowOpacity(frame, 0.01f);//初始化透明度
        frame.setVisible(true);
        frame.setAlwaysOnTop(true);//窗体置顶
        AWTUtilities.setWindowOpacity(frame, 1f);
        //添加关闭窗口的监听
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                frame.dispose();
            }
        });
        readMessage(frameEntity.getReadMessage());
        hide();
    }

    /**
     * 合成语音并朗读
     *
     * @param message
     */
    public void readMessage(String message) {
        try {

            // 初始化一个AipSpeech
            AipSpeech client = new AipSpeech(APP_ID, API_KEY, SECRET_KEY);

            // 可选：设置网络连接参数
            client.setConnectionTimeoutInMillis(2000);
            client.setSocketTimeoutInMillis(60000);
            // 调用接口
            TtsResponse res = client.synthesis(message, "zh", 1, null);
            byte[] data = res.getData();
            BufferedInputStream buffer = new BufferedInputStream(new ByteArrayInputStream(data));
            Player player = new Player(buffer);
            player.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hide() {
        float opacity = 100;
        while (true) {
            if (opacity < 2) {
                System.out.println();
                break;
            }
            opacity = opacity - 2;
            AWTUtilities.setWindowOpacity(frame, opacity / 100);
            try {
                Thread.sleep(20);
            } catch (Exception e1) {
            }
        }
        frame.dispose();
    }

    public static void main(String[] args) {
        FrameEntity frameEntity = new FrameEntity();
        frameEntity.setMessage("你好");
        frameEntity.setTitle("提示");
        TranslucentFrame translucentFrame = new TranslucentFrame();
        translucentFrame.drawFrame(frameEntity);
    }

}
