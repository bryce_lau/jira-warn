package com.kyee.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.atlassian.jira.rest.client.JiraRestClient;
import com.atlassian.jira.rest.client.NullProgressMonitor;
import com.atlassian.jira.rest.client.domain.BasicIssue;
import com.atlassian.jira.rest.client.domain.Field;
import com.atlassian.jira.rest.client.domain.Issue;
import com.atlassian.jira.rest.client.domain.SearchResult;
import com.kyee.common.poi.service.SXSSFExcelServiceImpl;
import com.kyee.entity.FrameEntity;
import com.kyee.entity.JiraWarnEntity;
import com.kyee.entity.Sheet;
import com.kyee.service.swingMessage.TranslucentFrame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.kyee.common.util.JiraUtil.login_jira;

/**
 * Created by BryceLau on 2018/1/19.
 */
@Component
public class JiraScheduled {
    @Autowired
    private Environment env;
    @Autowired
    private SXSSFExcelServiceImpl sxssfExcelService;
    @Autowired
    TranslucentFrame translucentFrame;

    /*
    private static final List<String> v2Keys = Arrays.asList(new String[]{"重构", "揭西", "安庆", "【V2】", "医保结算", "【v2版本】"});
    */
    public static final String IS_WARN = "1";
    public static final String IS_SALF = "2";
    public static final String IS_ERROR = "3";
    private static final Integer MAX_RESULTS = 1000;
    private static final String TITLE = "JIRA提示";
    private static final String MESSAGE_WARN = "重要的事情说三遍！您有Jira单即将到期，请关注！您有Jira单即将到期，请关注！您有Jira单即将到期，请关注！";
    private static final String MESSAGE_ERROR = "重要的事情说三遍！您有Jira单已到期，请尽快处理！您有Jira单已到期，请尽快处理！您有Jira单已到期，请尽快处理";
    private static final long ONE_Minute = 6 * 1000;
    private static final String ALL_UNRESOLVE_JQL = "assignee = currentUser() AND resolution = Unresolved order by updated DESC";
    private static final String blank = "    ";
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * 校验是否有Jira单到期
     */
    @Scheduled(fixedDelay = ONE_Minute)
    public void checkDelaySheet() throws Exception {
        FrameEntity frameEntity = new FrameEntity();
        frameEntity.setTitle(TITLE);
        List<JiraWarnEntity> jiraWarnEntityList = getJiraWarn();
        if (jiraWarnEntityList.stream().filter(i -> IS_ERROR.equals(i.getWarnStatus())).count() > 0) {
            frameEntity.setReadMessage(MESSAGE_ERROR);
        } else {
            if (jiraWarnEntityList.stream().filter(i -> IS_WARN.equals(i.getWarnStatus())).count() > 0) {
                frameEntity.setReadMessage(MESSAGE_WARN);
            }
        }
        String errorMessage = "<strong>已逾期Jira单:</strong>" + "<br/>" + String.join("", jiraWarnEntityList.stream().filter(i -> IS_ERROR.equals(i.getWarnStatus())).map(i -> i.getJiraMessage()).collect(Collectors.toList()).toArray(new String[0]));
        String warnMessage = "<br/>" + "<strong>近期可能逾期Jira单:</strong>" + "<br/>" + String.join("", jiraWarnEntityList.stream().filter(i -> IS_WARN.equals(i.getWarnStatus())).map(i -> i.getJiraMessage()).collect(Collectors.toList()).toArray(new String[0]));
        if (!StringUtils.isEmpty(frameEntity.getReadMessage())) {
            frameEntity.setMessage(errorMessage + warnMessage);
            translucentFrame.drawFrame(frameEntity);
        }
    }

    public List<JiraWarnEntity> getJiraWarn() throws Exception {
        List<JiraWarnEntity> result = new ArrayList<>();
        String userName = env.getProperty("name");
        String password = env.getProperty("password");
        final JiraRestClient restClient = login_jira(env.getProperty("uri"),userName, password);
        final NullProgressMonitor pm = new NullProgressMonitor();
        List<Object[]> excelData = new ArrayList<>();
        List<String> issueKeys = getAllSearchResultKey(restClient, pm, Arrays.asList(new String[]{ALL_UNRESOLVE_JQL}));
        issueKeys.stream().forEach(key -> {
            JiraWarnEntity jiraWarnEntity = new JiraWarnEntity();
            final Issue issue = restClient.getIssueClient().getIssue(key, pm);
            if (!ObjectUtils.isEmpty(issue.getField("customfield_13917"))) {
                Field field = issue.getField("customfield_13917");
                switch (isErrorDay(String.valueOf(field.getValue()))) {
                    case IS_WARN:
                        jiraWarnEntity.setWarnStatus(IS_WARN);
                        jiraWarnEntity.setJiraMessage("[" + issue.getKey() + "]" + issue.getSummary() + ";" + "<br/>");
                        break;
                    case IS_ERROR:
                        jiraWarnEntity.setWarnStatus(IS_ERROR);
                        jiraWarnEntity.setJiraMessage("[" + issue.getKey() + "]" + issue.getSummary() + ";" + "<br/>");
                        break;
                    default:
                }
                if (!ObjectUtils.isEmpty(jiraWarnEntity))
                    result.add(jiraWarnEntity);
            }

        });
        return result;
    }

    List<String> getAllSearchResultKey(JiraRestClient restClient, NullProgressMonitor pm, List<String> jqlList) {
        List<String> result = new ArrayList<>();
        jqlList.stream().forEach(key -> {
            SearchResult searchResult = restClient.getSearchClient().searchJql(key, MAX_RESULTS, 0, pm);
            Iterator<BasicIssue> basicIssues = searchResult.getIssues().iterator();
            while (basicIssues.hasNext()) {
                result.add(basicIssues.next().getKey());
            }
            if (searchResult.getMaxResults() > MAX_RESULTS) {
                int loopCount = Integer.valueOf(String.valueOf(Math.ceil(searchResult.getMaxResults() / MAX_RESULTS)));
                for (int i = 1; i < loopCount; i++) {
                    SearchResult searchResultLoop = restClient.getSearchClient().searchJql(key, MAX_RESULTS, MAX_RESULTS * i, pm);
                    Iterator<BasicIssue> basicIssuesLoop = searchResult.getIssues().iterator();
                    while (basicIssuesLoop.hasNext()) {
                        result.add(basicIssuesLoop.next().getKey());
                    }
                }
            }
        });
        return result;
    }

    public String isErrorDay(String date) {
        int delayDay = -3;
        String warndays = env.getProperty("warndays");
        if (org.apache.commons.lang.StringUtils.isNumeric(warndays)) {
            delayDay = -1 * (new BigDecimal(warndays).intValue());
        }
        String isError = IS_SALF;
        try {
            Date commitDate = simpleDateFormat.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(commitDate);
            calendar.set(Calendar.HOUR, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            if (new Date().after(calendar.getTime())) {
                isError = IS_ERROR;
            } else {
                calendar.add(Calendar.DAY_OF_YEAR, delayDay);
                System.out.println(simpleDateFormat.format(calendar.getTime()));
                if (new Date().after(calendar.getTime())) {
                    isError = IS_WARN;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isError;
    }
}
