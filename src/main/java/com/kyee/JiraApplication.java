/**
 * Created by BryceLau on 2018/1/19.
 */
package com.kyee;

import com.kyee.service.swingMessage.TranslucentFrame;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@ComponentScan("com.kyee")
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@PropertySource({"file:${user.dir}/config/jira.properties"})
public class JiraApplication {

    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext context = new SpringApplicationBuilder(JiraApplication.class).headless(false).run(args);
        TranslucentFrame appFrame = context.getBean(TranslucentFrame.class);
    }
}
